<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

# omf_pkg_nix_freeze
> A plugin for [Oh My Fish][omf].

[![Fish Shell Version][fish_version]][fish]
[![Oh My Fish Framework][omf_image]][omf]
[![Licence MIT][licence_mit_image]][mit]

<br/>

Help to freeze [`nix`][nix]'s channel for [`nix-shell`][nix-shell]

It works great with [`direnv`][direnv] and [`nixify`][nixify]

## Install

```fish
omf install https://gitlab.com/pinage404/omf_pkg_nix_freeze.git
```

## Usage

**Example:** Given that you have a `default.nix`'s file containing this

```nix
{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    python3
  ];
}
```

It's great !

But it's not **really** reproductible because the channel can be updated breaking your environment (by deleting package or updating it with a version including some breaking changes ...)

If you want to fix this potential issue, you'd like to freeze the channel for this file

This package is created to fix this

Just run :

```fish
nix_freeze
```

Then the `default.nix`'s file will looks like

```nix
{ pkgs ? import (builtins.fetchGit {
  name = "nixos-20.09-d105075a1fd870b1d1617a6008cb38b443e65433";
  url = "https://nixos.org/channels/nixos-20.09";
  ref = "refs/heads/nixos-20.09";
  rev = "d105075a1fd870b1d1617a6008cb38b443e65433";
}) {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    python3
  ];
}
```

There is also [a `sh` / `bash` script](./nix_freeze.sh) available

## License

[MIT][mit] © [pinage404][author] et [al][contributors]

[fish]:              https://fishshell.com
[fish_version]:      https://img.shields.io/badge/fish-v2.2.0-007EC7.svg?style=flat-square

[omf]:               https://www.github.com/oh-my-fish/oh-my-fish
[omf_image]:         https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square

[mit]:               ./LICENSE.md
[licence_mit_image]: https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square

[nix]:               https://nixos.org/
[nix-shell]:         https://nixos.wiki/wiki/Development_environment_with_nix-shell

[direnv]:            https://direnv.net/
[nixify]:            https://github.com/direnv/direnv/wiki/Nix#shell-function-to-quickly-setup-nix--direnv-in-a-new-project

[author]:            https://gitlab.com/pinage404
[contributors]:      https://gitlab.com/pinage404/omf_pkg_nix_freeze/graphs/master
