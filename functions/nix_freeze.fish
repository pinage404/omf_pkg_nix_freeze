#!/usr/bin/env fish
function nix_freeze \
    --description "Freeze nix's channel version" \
    --argument-names FILEPATH
    if not set --query FILEPATH[1]
        set FILEPATH "default.nix"
    end

    set CHANNEL "nixos"

    set VERSION (cat "/nix/var/nix/profiles/per-user/root/channels/$CHANNEL/.version")

    set URL (cat "$HOME/.nix-channels" | grep --regexp "\s+$CHANNEL\$" | cut --delimiter=' ' --fields=1)
    set REF "refs/heads/$CHANNEL-$VERSION"
    set REV (nixos-version --revision)
    set NAME "$CHANNEL-$VERSION-$REV"

    set NIXPKGS_FROZEN \
        "(builtins.fetchGit {" \
        "  name = \"$NAME\";" \
        "  url = \"$URL\";" \
        "  ref = \"$REF\";" \
        "  rev = \"$REV\";" \
        "})"
    set NIXPKGS_FROZEN (string join "\n" $NIXPKGS_FROZEN)

    sed --in-place "s#<nixpkgs>#$NIXPKGS_FROZEN#" $FILEPATH
end
